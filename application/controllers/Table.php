<?php 
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


class Table extends CI_Controller
{

	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
	}

	function index(){
		$this->load->view("table_view");
	}

	function urlid($id){
		$output = 'http://localhost:8080/all2/'.$id.'?key=123';
		return $output;
	}

	function http_request($url){
    	// persiapkan curl
    	$ch = curl_init(); 

    	// set url 
    	curl_setopt($ch, CURLOPT_URL, $url);
    

    	// return the transfer as a string 
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

    	// $output contains the output string 
    	$output = curl_exec($ch); 

    	// tutup curl 
    	curl_close($ch);      

    	// mengembalikan hasil curl
    	return $output;

	}	

	function ambil_data(){

		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$total=$this->db->count_all_results("TREMS_NP_SALDO");

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("BPNAME",$search);
		}


		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('BPNAME','DESC');
		$query=$this->db->get('TREMS_NP_SALDO');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("BPNAME",$search);
		$jum=$this->db->get('TREMS_NP_SALDO');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		$nomor_urut=$start+1;
		foreach ($query->result_array() as $data) {
			$IDNUMBER=$data['IDNUMBER'];
			$output['data'][]=array($data['IDNUMBER'],$data['BPNAME'],$data['UBIS'],number_format($data['SALDO_DC_TOTAL'],0, ',', '.'),number_format($data['SALDO_IDR_TOTAL'],0, ',', '.'),'<div><a id="'.$IDNUMBER.' " class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_add_new" onclick="showDetails(this)"> Detail</a></div>');
		$nomor_urut++;
		}

		echo json_encode($output);


	}

	function detail(){
		$customerNumber=$_GET["costumerNumber"];
		$customerNumber=trim($customerNumber);
		$customerNumber=$this->http_request($this->urlid($customerNumber));
		//$customerNumber=json_decode($customerNumber, TRUE);
		//mysqli_fetch_object($customerNumber);
		echo $customerNumber;
	}

}


 ?>