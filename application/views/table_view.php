<?php 
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

 ?>

<!DOCTYPE html>
<html>
<head>
	<title>TG Checker</title>
<script type="text/javascript" src="<?php echo base_url('assets/jquery.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatables/media/js/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatables/media/js/dataTables.bootstrap.min.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">

<style type="text/css">

.container{
	margin-top: 50px;
}

</style>

</head>
<body>
<br /><br />  
           <div class="container" style="width:1100px;">  
                <h2 align="center">TG Checking</h2>  
                <h3 align="center">Dashboard Tunggakan Telkom Group</h3>                 
                <br /><br />  
<div class="container">
<div class="row">
<div class="col-md-9">

<table class="table table-striped table-desa">
	<thead>
	<tr>
		<th style="width:50px">IDNUMBER</th>
		<th>BPNAME</th>
		<th>UBIS</th>
		<th>SALDO_DC_TOTAL</th>
		<th>SALDO_IDR_TOTAL</th>
		<th>DETAIL</th>
	</tr>
	</thead>
</table>

</div>
</div>
</div>


 <div class="modal fade" id="modal_add_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
           
            </script>
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3 class="modal-title" id="myModalLabel">Detail</h3>
                
    			
            </div>
            <form class="form-horizontal" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-xs-3" >RPART</label>
                        <div class="col-xs-8">
                            <input id="RPART" class="form-control" type="text"  readonly/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >PARTNER</label>
                        <div class="col-xs-8">
                            <input id="PARTNER" class="form-control" type="text"  readonly/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >IDNUMBER</label>
                        <div class="col-xs-8">
                            <input id="IDNUMBER" class="form-control" type="text"  readonly/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >CA</label>
                        <div class="col-xs-8">
                            <input id="CA" class="form-control" type="text"  readonly/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >NIPNAS</label>
                        <div class="col-xs-8">
                            <input id="NIPNAS" class="form-control" type="text"  readonly/>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-xs-3" >ACCOUNT_NAME</label>
                        <div class="col-xs-8">
                            <input id="ACCOUNT_NAME" class="form-control" type="text"  readonly/>
                        </div>
                    </div>
 
                    <div class="form-group">
                        <label class="control-label col-xs-3" >BPNAME</label>
                        <div class="col-xs-8">
                            <input id="BPNAME" class="form-control" type="text"  readonly/>
                        </div>
                    </div>
                
                <div class="form-group">
                        <label class="control-label col-xs-3" >INEXT</label>
                        <div class="col-xs-8">
                            <input id="INEXT" class="form-control" type="text"  readonly/>
                        </div>
                </div> 

                <div class="form-group">
                        <label class="control-label col-xs-3" >SUBSIDIARY</label>
                        <div class="col-xs-8">
                            <input id="SUBSIDIARY" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >UBIS</label>
                        <div class="col-xs-8">
                            <input id="UBIS" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >SEGMEN</label>
                        <div class="col-xs-8">
                            <input id="SEGMEN" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >SUBSEGMEN</label>
                        <div class="col-xs-8">
                            <input id="SUBSEGMEN" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >BISNIS_AREA</label>
                        <div class="col-xs-8">
                            <input id="BISNIS_AREA" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >BUSINESS_SHARE</label>
                        <div class="col-xs-8">
                            <input id="BUSINESS_SHARE" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >DIVISI</label>
                        <div class="col-xs-8">
                            <input id="DIVISI" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >WITEL</label>
                        <div class="col-xs-8">
                            <input id="WITEL" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >PRODUK</label>
                        <div class="col-xs-8">
                            <input id="PRODUK" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >BILL_CURR</label>
                        <div class="col-xs-8">
                            <input id="BILL_CURR" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >AWTYPE</label>
                        <div class="col-xs-8">
                            <input id="AWTYPE" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >BILL_DC_LAST</label>
                        <div class="col-xs-8">
                            <input id="BILL_DC_LAST" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >BILL_DC_CURR</label>
                        <div class="col-xs-8">
                            <input id="BILL_DC_CURR" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >BILL_DC_TOTAL</label>
                        <div class="col-xs-8">
                            <input id="BILL_DC_TOTAL" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >COLL_DC_TOTAL</label>
                        <div class="col-xs-8">
                            <input id="COLL_DC_TOTAL" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >SALDO_DC_TOTAL</label>
                        <div class="col-xs-8">
                            <input id="SALDO_DC_TOTAL" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >SALDO_DC_1</label>
                        <div class="col-xs-8">
                            <input id="SALDO_DC_1" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >SALDO_DC_2</label>
                        <div class="col-xs-8">
                            <input id="SALDO_DC_2" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >SALDO_DC_3</label>
                        <div class="col-xs-8">
                            <input id="SALDO_DC_3" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >SALDO_DC_4</label>
                        <div class="col-xs-8">
                            <input id="SALDO_DC_4" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >SALDO_DC_5</label>
                        <div class="col-xs-8">
                            <input id="SALDO_DC_5" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >L11_SALDO</label>
                        <div class="col-xs-8">
                            <input id="L11_SALDO" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >IS_SALDO</label>
                        <div class="col-xs-8">
                            <input id="IS_SALDO" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >BILL_IDR_LAST</label>
                        <div class="col-xs-8">
                            <input id="BILL_IDR_LAST" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >BILL_IDR_CURR</label>
                        <div class="col-xs-8">
                            <input id="BILL_IDR_CURR" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >BILL_IDR_TOTAL</label>
                        <div class="col-xs-8">
                            <input id="BILL_IDR_TOTAL" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >COLL_IDR_TOTAL</label>
                        <div class="col-xs-8">
                            <input id="COLL_IDR_TOTAL" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >SALDO_IDR_TOTAL</label>
                        <div class="col-xs-8">
                            <input id="SALDO_IDR_TOTAL" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >SALDO_IDR_1</label>
                        <div class="col-xs-8">
                            <input id="SALDO_IDR_1" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >SALDO_IDR_2</label>
                        <div class="col-xs-8">
                            <input id="SALDO_IDR_2" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >SALDO_IDR_3</label>
                        <div class="col-xs-8">
                            <input id="SALDO_IDR_3" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >SALDO_IDR_4</label>
                        <div class="col-xs-8">
                            <input id="SALDO_IDR_4" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-xs-3" >SALDO_IDR_5</label>
                        <div class="col-xs-8">
                            <input id="SALDO_IDR_5" class="form-control" type="text"  readonly/>
                        </div>
                </div>

                <div class="modal-footer">
                    
                </div>
            </form>
            </div>
            </div>
        </div>
        <!--END MODAL ADD BARANG-->


<script type="text/javascript">
$(".table-desa").DataTable({
	ordering: false,
	processing: true,
	serverSide: true,
	ajax: {
	  url: "<?php echo base_url('index.php/table/ambil_data') ?>",
	  type:'POST',
	}

});


</script>
<script type="text/javascript">
function showDetails(button){
    var costumerNumber = button.id;
    console.log(costumerNumber);
    $.ajax({
        url: "<?php echo base_url('index.php/table/detail') ?>",
        type:"GET",
        data: {"costumerNumber":costumerNumber},
        success: function(response){
            var detail = JSON.parse(response);
            $("#RPART").val(detail[0].RPART);
            $("#PARTNER").val(detail[0].PARTNER);
            $("#IDNUMBER").val(detail[0].IDNUMBER);
            $("#CA").val(detail[0].CA);
            $("#NIPNAS").val(detail[0].NIPNAS);
            $("#ACCOUNT_NAME").val(detail[0].ACCOUNT_NAME);
            $("#BPNAME").val(detail[0].BPNAME);
             $("#INEXT").val(detail[0].INEXT);
            $("#SUBSIDIARY").val(detail[0].SUBSIDIARY);
            $("#UBIS").val(detail[0].UBIS);
            $("#SEGMEN").val(detail[0].SEGMEN);
            $("#SUBSEGMEN").val(detail[0].SUBSEGMEN);
            $("#BISNIS_AREA").val(detail[0].BISNIS_AREA);
            $("#BUSINESS_SHARE").val(detail[0].BUSINESS_SHARE);
            $("#DIVISI").val(detail[0].DIVISI);
            $("#WITEL").val(detail[0].WITEL);
            $("#PRODUK").val(detail[0].PRODUK);
            $("#BILL_CURR").val(detail[0].BILL_CURR);
            $("#AWTYPE").val(detail[0].AWTYPE);
            $("#BILL_DC_LAST").val(detail[0].BILL_DC_LAST);
            $("#BILL_DC_CURR").val(detail[0].BILL_DC_CURR);
            $("#BILL_DC_TOTAL").val(detail[0].BILL_DC_TOTAL);
            $("#COLL_DC_TOTAL").val(detail[0].COLL_DC_TOTAL);
            $("#SALDO_DC_TOTAL").val(detail[0].SALDO_DC_TOTAL);
            $("#SALDO_DC_TOTAL").val(detail[0].SALDO_DC_TOTAL);
            $("#SALDO_DC_1").val(detail[0].SALDO_DC_1);
            $("#SALDO_DC_2").val(detail[0].SALDO_DC_2);
            $("#SALDO_DC_3").val(detail[0].SALDO_DC_3);
            $("#SALDO_DC_4").val(detail[0].SALDO_DC_4);
            $("#SALDO_DC_5").val(detail[0].SALDO_DC_5);
            $("#L11_SALDO").val(detail[0].L11_SALDO);
            $("#IS_SALDO").val(detail[0].IS_SALDO);
            $("#BILL_IDR_LAST").val(detail[0].BILL_IDR_LAST);
            $("#BILL_IDR_CURR").val(detail[0].BILL_IDR_CURR);
            $("#BILL_IDR_TOTAL").val(detail[0].BILL_IDR_TOTAL);
            $("#COLL_IDR_TOTAL").val(detail[0].COLL_IDR_TOTAL);
            $("#SALDO_IDR_TOTAL").val(detail[0].SALDO_IDR_TOTAL);
            $("#SALDO_IDR_1").val(detail[0].SALDO_IDR_1);
            $("#SALDO_IDR_2").val(detail[0].SALDO_IDR_2);
            $("#SALDO_IDR_3").val(detail[0].SALDO_IDR_3);
            $("#SALDO_IDR_4").val(detail[0].SALDO_IDR_4);
            $("#SALDO_IDR_5").val(detail[0].SALDO_IDR_5);

        }
    });    
    
}</script>

</body>
</html>